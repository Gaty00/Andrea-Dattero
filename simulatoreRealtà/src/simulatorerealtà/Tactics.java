package simulatorerealtà;

/**
 *
 * @author Andrea
 */
public interface Tactics {
    void attach(personaggi p);
    void defend(personaggi p);
    String confronter(personaggi p,personaggi p1);
}
