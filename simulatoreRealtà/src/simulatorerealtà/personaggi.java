package simulatorerealtà;

/**
 *
 * @author Andrea
 */
public class personaggi implements Tactics {

    String name, obj;
    int strength, agility, intelligence, life, fisico;
    static int turno = 0;

    personaggi(String name, int strength, int agility, int intelligence, int fisico, String ob, int life) {
        this.name = name;
        this.strength = strength;
        this.agility = agility;
        this.intelligence = intelligence;
        this.obj = obj;
        this.life = life;
        this.fisico = fisico;
    }

    @Override
    public void attach(personaggi p) {
        if (this.life >= 1) {
            if (p.fisico > this.strength) {
                p.life -= (p.fisico - this.strength);
            } else {
                p.life += (p.fisico - this.strength);
            }
        }
        this.fisico -= p.strength;
    }

    @Override
    public void defend(personaggi p) {
        if (this.agility >= p.agility) {
            if (p.strength < this.fisico) {
                this.life += (this.fisico - p.strength) + 2 / 3;
            } else {
                this.life -= (this.fisico - p.strength) * 2 / 3;
            }
        }
        this.fisico -= p.strength;
        if (this.life <= 0) {
            this.life = 0;
            System.out.println(this.name + " è morto");
        }
    }

    @Override
    public String confronter(personaggi p, personaggi p1) {
        boolean ris[] = new boolean[3];
        int[] valoriP1 = {p1.strength, p1.agility, p1.intelligence};
        int[] valoriP = {p.strength, p.agility, p.intelligence};
        ris[0] = valoriP[0] > valoriP1[0];
        ris[1] = valoriP[1] > valoriP1[1];
        ris[2] = valoriP[2] > valoriP1[2];
        int poss = 1;
        for (int i = 0; i < ris.length; i++) {
            if (ris[i] == true) {
                poss++;
            } else {
                poss--;
            }
        }
        System.out.println(poss);
        if (poss >= 2) {
            return p.name + " potrebbe ucciderlo " + p1.name;
        } else {
            return p.name + " è inferiore " + p1.name;
        }
    }

    int IAggressiva(personaggi p) {
        if (p.life > 0) {
            attach(p);
            int def = (int) (Math.random() * this.intelligence);
            if (def > this.intelligence - 2) {
                defend(p);
            }
        }
        turno++;
        return turno;
    }
}
