package simulatorerealtà;

/**
 *
 * @author Andrea
 */
public class event {

    public static String turns(personaggi p, personaggi p1) {
        personaggi.turno = 0;
        if (p.intelligence >= p1.intelligence) {
            if (personaggi.turno % 2 == 0) {
                p.IAggressiva(p1);
            }
            if (personaggi.turno % 2 != 0) {
                p1.IAggressiva(p);
            }
        } else {
            if (personaggi.turno % 2 == 0) {
                p1.IAggressiva(p);
            }
            if (personaggi.turno % 2 != 0) {
                p.IAggressiva(p1);
            }

        }
        if (p1.life > 0 && p.life > 0) {
            System.out.println("vita di andrea: " + p.life + " vita di nella " + p1.life);
            turns(p, p1);
        }
        
        if (p.life <= 0) {
            return p.name + " è morto";
        } else if (p1.life <= 0) {
            return p1.name + " è morto";
        }
        return null;
    }
}
