package simulatorerealtà;

/**
 *
 * @author Andrea
 */
public class oggetti {

    String nameP;
    int damage, heal, protection;
    Object o;

    oggetti(String nameP, int damage, Object o) {
        this.nameP = nameP;
        this.damage = damage;
        this.o = o;
    }

    oggetti(String nameP, Object o, int heal) {
        this.nameP = nameP;
        this.heal = heal;
        this.o = o;
    }

    oggetti(Object o, String nameP, int protection) {
        this.nameP = nameP;
        this.protection = protection;
        this.o = o;
    }

}
