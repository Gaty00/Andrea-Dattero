package pkg2048;

import java.util.ArrayList;
import zuclib.*;

/**
 *
 * @author andrea
 */
public class Main {

    static ArrayList<Double> posizioni = new ArrayList<>();
    static ArrayList<Double> posOccupate = new ArrayList<>();
    static ArrayList<String> numeri = new ArrayList<>();

    public static void main(String[] args) {
        startCampo(4);
        posOccupate = startNumeri(posizioni);
        generaNumeri(posOccupate);
        while (!GraficaSemplice.tastoPremuto()) {
        }
        if (GraficaSemplice.premutoTasto(87)) {
            posOccupate = moveW(posOccupate, numeri);
        }
    }

    static ArrayList<Double> startCampo(int num) {
        GraficaSemplice.setFinestra(1500, 650, null);
        double x = 0.2;
        double y = 0.2;
        double add = 0.2;
        int i = 0;
        int pos = 0;
        while (num > 0) {
            for (i = 0; i < 4; i++) {
                GraficaSemplice.quadrato(x, y, 0.2);
                posizioni.add(x);
                posizioni.add(y);
                y += add;
                pos += 2;
            }
            x += 0.2;
            add = -add;
            y += add;
            i = 0;
            num--;
        }
        System.out.println(posizioni);
        return posizioni;
    }

    static ArrayList<Double> startNumeri(ArrayList<Double> posizioni) {
        int selez = (int) (Math.random() * posizioni.size());
        int pos = (int) (Math.random() * posizioni.size());
        ArrayList<Double> posizOccupate = new ArrayList<>();
        if (selez < (posizioni.size() / 2)) {
            if (pos % 2 == 0) {
                GraficaSemplice.testo(posizioni.get(pos), posizioni.get(pos + 1), "2");
                posizOccupate.add(posizioni.get(pos));
                posizOccupate.add(posizioni.get(pos + 1));
            } else {
                GraficaSemplice.testo(posizioni.get(pos - 1), posizioni.get(pos), "2");
                posizOccupate.add(posizioni.get(pos - 1));
                posizOccupate.add(posizioni.get(pos));
            }
        } else {
            if (pos % 2 == 0) {
                GraficaSemplice.testo(posizioni.get(pos), posizioni.get(pos + 1), "4");
                posizOccupate.add(posizioni.get(pos));
                posizOccupate.add(posizioni.get(pos + 1));
            } else {
                GraficaSemplice.testo(posizioni.get(pos - 1), posizioni.get(pos), "4");
                posizOccupate.add(posizioni.get(pos - 1));
                posizOccupate.add(posizioni.get(pos));
            }
        }

        return posOccupate;
    }

    static ArrayList<Double> generaNumeri(ArrayList<Double> posOccupate) {
        int selez = (int) (Math.random() * posizioni.size());
        int pos = (int) (Math.random() * posizioni.size());
        ArrayList<Double> posizioniOccupate = new ArrayList<>();
        if (selez < (posizioni.size() / 2)) {
            if (pos % 2 == 0 && posOccupate != posizioniOccupate) {
                GraficaSemplice.testo(posizioni.get(pos), posizioni.get(pos + 1), "2");
                numeri.add("2");
                posizioniOccupate.add(posizioni.get(pos));
                posizioniOccupate.add(posizioni.get(pos + 1));
            } else if (pos % 2 != 0) {
                GraficaSemplice.testo(posizioni.get(pos - 1), posizioni.get(pos), "2");
                numeri.add("2");
                posizioniOccupate.add(posizioni.get(pos - 1));
                posizioniOccupate.add(posizioni.get(pos));
            }
        } else {
            if (pos % 2 == 0 && posOccupate != posizioniOccupate) {
                GraficaSemplice.testo(posizioni.get(pos), posizioni.get(pos + 1), "4");
                numeri.add("4");
                posizioniOccupate.add(posizioni.get(pos));
                posizioniOccupate.add(posizioni.get(pos + 1));
            } else if (pos % 2 != 0) {
                GraficaSemplice.testo(posizioni.get(pos - 1), posizioni.get(pos), "4");
                numeri.add("4");
                posizioniOccupate.add(posizioni.get(pos - 1));
                posizioniOccupate.add(posizioni.get(pos));
            }
        }

        return posizioniOccupate;
    }

    static ArrayList<Double> moveW(ArrayList<Double> posOccupate, ArrayList<String> numeri) {
        double pos = 0.8;
        for (int i = 1; i < posOccupate.size(); i += 2) {
            posOccupate.set(i, pos);
            GraficaSemplice.testo(posOccupate.get(i - 1), posOccupate.get(i), "ciao");
            pos -= 0.2;
        }
        startCampo(4);
        return posOccupate;
    }
}
