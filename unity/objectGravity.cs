﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectGravity : MonoBehaviour {
    public GameObject star;
    public float rotationVelocity, max = 12f;
    public Transform Planet, Star;
    private Rigidbody2D r;
    private int i = 0;
    private bool c=false;
    public static List<GameObject> arrayOnGravity = new List<GameObject>(); 
    int posArrayOnGravity = 0;
    private void Start()
    {
        r = gameObject.GetComponent<Rigidbody2D>();
        r.velocity = Random.onUnitSphere * max;
    }



    void Update () {
        
        transform.RotateAround(Star.position, new Vector3(0, 0, 20), rotationVelocity * Time.deltaTime);
        Orbital();
        
    }



    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("cerchio") && i < 1) {
            Planet = coll.transform;
            c = true;
            i++;
            arrayOnGravity.Add(star);
        }
        else if (coll.gameObject.CompareTag("colliders")) {
            r.velocity = Random.onUnitSphere * max;
        }
    }




    void Orbital()
    {
        if (Planet != null)
        {
            r.velocity = Vector3.zero;
            if (c==true) {
                Star.transform.position = Vector3.Lerp(star.transform.position, Planet.position, 5*Time.deltaTime);
            }
            transform.RotateAround(Planet.position, new Vector3(0, 0, 2), rotationVelocity * Time.deltaTime);
        }
        c = false;
    }
    
}
