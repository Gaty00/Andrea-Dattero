﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newPlanet2 : MonoBehaviour {
    public GameObject newPianeta;
    public GameObject[] arrayDiGameObject = new GameObject[1];
    public GameObject questoGameObject;
    public GameObject nuovoPianeta;
    private void Start()
    {
        arrayDiGameObject[0] = questoGameObject;
    }
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("quadrato") || coll.gameObject.CompareTag("quadrato1")) {
            if (objectGravity.arrayOnGravity[0] != null && objectGravity.arrayOnGravity[1] != null)
            {
                if (newPlanet.randomizer > 2)
                {
                    if (objectGravity.arrayOnGravity[0].CompareTag(objectGravity.arrayOnGravity[1].tag) == true)
                    {
                        spawnerOnParity(objectGravity.arrayOnGravity[0]);
                        objectGravity.arrayOnGravity[1] = null;
                        Destroy(coll.gameObject);
                    }
                    else if (objectGravity.arrayOnGravity[0] != null && objectGravity.arrayOnGravity[1] != null)
                    {
                        spawner();
                        Destroy(coll.gameObject);
                    }
                }
            }
        }

    }
    public void spawner()
    {
        newPianeta = Instantiate(arrayDiGameObject[0], new Vector3(transform.position.x, transform.position.y), Quaternion.identity);
        newPianeta.GetComponent<SpriteRenderer>().color = Random.ColorHSV();
        Destroy(gameObject);
    }
    void spawnerOnParity(GameObject conBooltrue)
    {
        newPianeta = Instantiate(nuovoPianeta, new Vector3(conBooltrue.transform.position.x, conBooltrue.transform.position.y), Quaternion.identity);
        newPianeta.GetComponent<SpriteRenderer>().color = Random.ColorHSV();
        Destroy(gameObject);
    }
}
