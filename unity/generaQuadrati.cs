﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generaQuadrati : MonoBehaviour
{
    public int nQuadrati;
    private GameObject quadratoSpawnato;
    public GameObject[] quadrati;
    private float x, y, z;
    void Start()
    {
        for (int i = 0; i < nQuadrati; i++)
        {
            x = Random.Range(-4, 4);
            y = Random.Range(-4, 4);
            if (x>=-2 && x<=2) {
                x = Random.Range(-4, 4);
                if (y >= -2 && y <= 2)
                {
                    y = Random.Range(-4, 4);
                }
            }
            quadratoSpawnato = Instantiate(quadrati[Random.Range(0, quadrati.Length)], new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
        }
    }
}
